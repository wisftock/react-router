import React from 'react';

import {
  BrowserRouter,
  NavLink,
  Navigate,
  Route,
  Routes,
} from 'react-router-dom';

import logo from '../logo.svg';

const Navigation = () => {
  return (
    <BrowserRouter>
      <div className='main-layout'>
        <nav>
          <img src={logo} alt='logo' />
          <ul>
            <li>
              <NavLink
                to='/'
                className={({ isActive }) => (isActive ? 'nav-active' : '')}
              >
                Home
              </NavLink>
              <NavLink
                to='/about'
                className={({ isActive }) => (isActive ? 'nav-active' : '')}
              >
                about
              </NavLink>
              <NavLink
                to='/user'
                className={({ isActive }) => (isActive ? 'nav-active' : '')}
              >
                user
              </NavLink>
            </li>
          </ul>
        </nav>

        <Routes>
          <Route path='about' element={<h1>About Page</h1>} />
          <Route path='user' element={<h1>Users Page</h1>} />
          <Route path='/' element={<h1>Home Page</h1>} />

          <Route path='/*' element={<Navigate to='/home' replace />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
};

export default Navigation;
